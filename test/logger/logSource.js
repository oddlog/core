import should from "should";
import {refresh} from "@oddlog/environment";

import {createLogger} from "../../lib";

describe("logger", () => {
  describe("logSources", () => {
    let logger = null, store;

    afterEach((done) => logger == null || logger.scope.closed ? done() : logger.close(done));

    it("should fallback to whether any env variable matches (matching debug)", () => {
      store = [];
      process.env.DEBUG = "some-id";
      refresh();
      logger = createLogger("some-id", {transports: [{store}]});
      logger.info("test");
      should(store[0]._source).not.be.null();
      Reflect.deleteProperty(process.env, "DEBUG");
      refresh();
    });

    it("should fallback to whether any env variable matches (matching trace)", () => {
      store = [];
      process.env.TRACE = "some-id";
      refresh();
      logger = createLogger("some-id", {transports: [{store}]});
      logger.info("test");
      should(store[0]._source).not.be.null();
      Reflect.deleteProperty(process.env, "TRACE");
      refresh();
    });

    it("should fallback to whether any env variable matches (not set)", () => {
      store = [];
      logger = createLogger("some-id", {transports: [{store}]});
      logger.info("test");
      should(store[0]._source).be.null();
    });

    it("should fallback to whether any env variable matches (not matching)", () => {
      store = [];
      process.env.DEBUG = "some-other-id";
      refresh();
      logger = createLogger("some-id", {transports: [{store}]});
      logger.info("test");
      should(store[0]._source).be.null();
      Reflect.deleteProperty(process.env, "DEBUG");
      refresh();
    });

    it("should be respected if set within logger options (matching, set to false)", () => {
      store = [];
      process.env.DEBUG = "some-id";
      refresh();
      logger = createLogger("some-id", {logSources: false, transports: [{store}]});
      logger.info("test");
      should(store[0]._source).be.null();
      Reflect.deleteProperty(process.env, "DEBUG");
      refresh();
    });

    it("should be respected if set within logger options (not matching, set to true)", () => {
      store = [];
      process.env.DEBUG = "some-other-id";
      refresh();
      logger = createLogger("some-id", {logSources: true, transports: [{store}]});
      logger.info("test");
      should(store[0]._source).not.be.null();
      Reflect.deleteProperty(process.env, "DEBUG");
      refresh();
    });
  });
});

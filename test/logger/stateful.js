import {createLogger, VERBOSE, WARN} from "../../lib";

describe("logger", () => {
  describe("stateful", () => {
    let logger, store;

    beforeEach(() => {
      store = [];
      logger = createLogger("some-id", {level: VERBOSE, transports: [{store}]});
    });
    afterEach((done) => logger.scope.closed ? done() : logger.close(done));

    it("should respect the fallback", () => {
      logger.getLevel().should.be.equal(VERBOSE);
      logger.log("pew");
      store.length.should.be.equal(1);
      store[0].level.should.be.equal(VERBOSE);
      logger
        .setLevel(WARN)
        .log("test")
        .log("test2");
      store.length.should.be.equal(3);
      store[1].level.should.be.equal(WARN);
      store[2].level.should.be.equal(WARN);
      logger.getLevel().should.be.equal(WARN);
      logger.done();
      logger.getLevel().should.be.equal(VERBOSE);
      logger.setWarn();
      logger.getLevel().should.be.equal(WARN);
    });
  });
});

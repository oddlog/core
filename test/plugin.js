import sinon from "sinon";

import {__pluginData, createLogger, mixin} from "../lib/index";
import DummyWriteStream from "./_util/DummyWriteStream";

describe("plugin", () => {

  let logger;

  beforeEach(() => {
    logger = createLogger("some-id", {transports: [{stream: new DummyWriteStream()}]}, {hello: "world"});
  });
  afterEach((done) => logger.scope.closed ? done() : logger.close(done));

  it("should call the plugin", () => {
    const plugin = sinon.spy();
    mixin(plugin);
    mixin(plugin);
    plugin.should.be.calledOnce();
    plugin.should.be.calledWith(__pluginData);
  });

  it("should work with init call", () => {
    const plugin = {init: sinon.spy()};
    mixin(plugin);
    plugin.init.should.be.calledWith(__pluginData);
  });

  it("should throw on invalid input", () => {
    mixin.bind(null, {}).should.throw();
  });

});

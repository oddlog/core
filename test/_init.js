import {mixin} from "../lib";
import {plugin as memoryTransportPlugin} from "@oddlog/transport-memory";

mixin(memoryTransportPlugin);

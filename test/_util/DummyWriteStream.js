import stream from "stream";

export default class DummyWriteStream extends stream.Writable {

  constructor(instant) {
    super();
    this.instant = instant !== false;
    this.received = "";
  }

  // noinspection JSMethodCanBeStatic
  _write(chunk, encoding, done) {
    this.received += chunk.toString();
    this.instant ? done() : process.nextTick(done);
  }

  // noinspection JSMethodCanBeStatic
  _writev(chunks, callback) { callback(null); }

}

import MemoryTransport from "@oddlog/transport-memory";

export default class DummyTransport extends MemoryTransport {

  constructor(options) { super(options || {}, []); }

  attach(logger) { MemoryTransport.prototype.attach.call(this, logger); }

}

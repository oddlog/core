import {createLogger} from "../../lib";
import {assignAll, clone} from "@oddlog/utils";

describe("payload", () => {
  describe("without granted ownership", () => {
    const loggerPldClone = {lorem: "ipsum"};
    let logger, store, loggerPld;

    beforeEach(() => {
      store = [];
      loggerPld = clone(loggerPldClone);
      logger = createLogger("some-id", {ownPayload: false, transports: [{store}]}, loggerPld);
    });
    afterEach((done) => logger.scope.closed ? done() : logger.close(done));

    it("should not (shallow) alter the application payload object", () => {
      const childPld = {ipsum: "dolor"}, logPld = {dolor: "sit"};
      logger.child(false, childPld).info(false, logPld, "test");
      store[0].getPayload().should.be.deepEqual(assignAll({}, loggerPldClone, childPld, logPld));
      loggerPld.should.be.deepEqual(loggerPldClone);
      childPld.should.be.deepEqual({ipsum: "dolor"});
      logPld.should.be.deepEqual({dolor: "sit"});
    });

    it("should not be altered by the application after method calls", () => {
      const childPld = {ipsum: "dolor"}, logPld = {dolor: "sit"};
      const expectedPayload = assignAll({}, loggerPldClone, childPld, logPld);
      loggerPld.loggerModification = true;
      const child = logger.child(false, childPld);
      childPld.childModification = true;
      child.info(false, logPld, "test");
      logPld.logModification = true;
      store[0].getPayload().should.be.deepEqual(expectedPayload);
    });
  });
});

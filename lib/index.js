import Log from "@oddlog/record";

import Logger, {createLogger} from "./Logger";
import LoggerScope from "./LoggerScope";
import {createTransport, defineType} from "./services/transports";
import * as stdTransformer from "./services/transformer";
import {define as defineLogger, get as getLogger} from "./services/store";
import {MAPPED as LEVELS} from "@oddlog/levels";

const PLUGINS = new WeakSet();

const PLUGIN_EXPORTS = {
  // transformer
  stdTransformer,
  // classes
  Logger, LoggerScope, Log,
  // creation functions
  createLogger, createTransport,
  // singleton functions
  getLogger, defineLogger,
  // type definition
  defineType,
  // bulk actions
  eachScopeOnce, closeAll,
  // plug-in
  mixin,
};

/*===================================================== Exports  =====================================================*/

export default getLogger;

export {
  // constants
  PLUGIN_EXPORTS as __pluginData, LEVELS as levels, stdTransformer,
  // main classes
  Logger, LoggerScope, Log,
  // creation functions
  createLogger, createTransport,
  // singleton functions
  getLogger, defineLogger,
  // type definition
  defineType,
  // bulk actions
  eachScopeOnce, closeAll,
  // plug-in
  mixin,
};

export const SILENT = LEVELS.silent;
export const TRACE = LEVELS.trace;
export const DEBUG = LEVELS.debug;
export const VERBOSE = LEVELS.verbose;
export const INFO = LEVELS.info;
export const WARN = LEVELS.warn;
export const ERROR = LEVELS.error;
export const FATAL = LEVELS.fatal;

/*==================================================== Functions  ====================================================*/

/**
 * Initializes the given plugin(s) with this library.
 *
 * @param {?Plugin|Plugin[]} plugins The plugin(s) to initialize.
 */
function mixin(plugins) { // todo allow {plugin, options}[] and options parameter
  if (!Array.isArray(plugins)) { plugins = [plugins]; }
  for (let i = 0; i < plugins.length; i++) {
    const plugin = plugins[i];
    if (plugin == null) { continue; }
    if (!PLUGINS.has(plugin)) {
      if (typeof plugin.init === "function") {
        plugin.init(PLUGIN_EXPORTS);
      } else if (typeof plugin === "function") {
        plugin(PLUGIN_EXPORTS);
      } else {
        throw new Error("Plugin '" + plugin + "' is not compatible with this version of oddlog");
      }
      PLUGINS.add(plugin);
    }
  }
}

// todo if no loggers are passed, use all loggers within singleton store

/**
 * Closes all given loggers and their children. Calls the given callback when all remaining logs are flushed to their
 * respective destination.
 *
 * A closed logger will no longer pass logs to its transports.
 *
 * @param {Boolean} [failSilent=true] Whether to silently ignore future incoming logs. If `false`, an Error will be
 *                  thrown by called logging methods.
 * @param {Logger[]} loggers The loggers to close.
 * @param {Function} cb The callback.
 * @see Logger#close
 */
function closeAll(failSilent, loggers, cb) {
  if (typeof failSilent !== "boolean") { [failSilent, loggers, cb] = [void 0, failSilent, loggers]; }
  let remaining = loggers.length;
  const _len = loggers.length;
  for (let i = 0; i < _len; i++) { loggers[i].close(failSilent, next); }

  function next() { if (!--remaining) { cb(); } }
}

/**
 * Calls the given callback once all scopes of the passed loggers have emitted the given event once. This function is
 * best called with root loggers and/or child loggers with dedicated scopes only.
 *
 * @param {Logger[]} loggers The loggers to listen for given event.
 * @param {String} event The name of the event to listen for.
 * @param {Function} cb The callback.
 * @see LoggerScope
 */
function eachScopeOnce(loggers, event, cb) {
  let remaining = loggers.length;
  const _len = loggers.length;
  for (let i = 0; i < _len; i++) { loggers[i].scope.once(event, next); }

  function next() { if (!--remaining) { cb(); } }
}

import {assign, assignWeak, clone} from "@oddlog/utils";

/*===================================================== Exports  =====================================================*/

export default attach;

/*==================================================== Functions  ====================================================*/

function attach(Logger) {

  /**
   * Adds the properties of given additional payload to the logger payload.
   *
   * @param {Boolean} [ownPayload=this.ownChildPayloads] Whether the payload is allowed to be modified by the logger.
   * @param {?Object} payload The additional payload to merge into logger payload.
   * @returns {Logger} Identity.
   */
  Logger.prototype.mergePayload = function (ownPayload, payload) {
    if (typeof ownPayload !== "boolean") {
      payload = ownPayload;
      ownPayload = this.ownChildPayloads;
    }
    if (payload === null) {
      this.__hasMergedPayload = true;
      this.__mergedPayload = null;
    } else if (payload !== void 0) {
      if (this.__hasMergedPayload) {
        if (this.transformer != null && !this._singleTransform) {
          payload = this._transformPayload(ownPayload ? payload : clone(payload));
          ownPayload = true;
        }
        if (this.__mergedPayload == null) {
          this.__mergedPayload = ownPayload ? payload : clone(payload);
        } else {
          assign(this.__mergedPayload, payload);
        }
      } else {
        if (this._payload == null) {
          this._payload = ownPayload ? payload : clone(payload);
        } else {
          assign(this._payload, payload);
        }
      }
    }
    return this;
  };

  /**
   * Returns and caches the full payload of the logger merged into the parent payload. Iff singleTransform is set to
   * false, the merged payload will also be transformed.
   * The resulting object is not allowed to be modified any further. It could be the same object as the parents
   * (non-transformed) payload.
   *
   * @returns {?Object|undefined} The merged (and eventually transformed) payload.
   */
  Logger.prototype.getPreparedPayload = function () {
    if (this.__hasMergedPayload) { return this.__mergedPayload; }
    this.__hasMergedPayload = true;
    let payload = this._payload;
    const inheritPayload = this._parentPayload;
    if (payload === void 0) { return this.__mergedPayload = inheritPayload; }
    if (payload === null) { return this.__mergedPayload = payload; }
    const transform = this.transformer != null && !this._singleTransform;
    if (inheritPayload == null) { return this.__mergedPayload = transform ? this._transformPayload(payload) : payload; }
    if (transform) { payload = this.__mergedPayload = this._transformPayload(payload); }
    return this.__mergedPayload = assignWeak(payload, inheritPayload);
  };

  /**
   * Performs a merge of the inherited payload with message payload. The inherited payload is assumed to already be
   * transformed iff singleTransform is false.
   *
   * @param {?Object} [inheritPayload] The parent payload payload.
   * @param {?Object} [payload] The message payload; Its values get transformed and may overwrite values of
   *        inheritPayload. If it is null, the result will be null as well.
   * @param {Boolean} ownPayload Whether the payload is allowed to be modified by the logger.
   * @returns {?Object|undefined} The transformed and merged payload; Undefined if both payloads are undefined.
   * @private
   */
  Logger.prototype._getFinalPayload = function (inheritPayload, payload, ownPayload) {
    if (payload === void 0) {
      if (inheritPayload == null) { return inheritPayload; }
      if (this._singleTransform && this.transformer != null) {
        return this._transformPayload(clone(inheritPayload));
      }
      return inheritPayload;
    }
    if (payload === null) { return payload; }
    if (inheritPayload == null) {
      if (this.transformer != null) {
        //noinspection JSCheckFunctionSignatures
        return this._transformPayload(ownPayload ? payload : clone(payload));
      }
      return payload;
    }
    if (!ownPayload) { payload = clone(payload); }
    if (this.transformer != null) {
      if (this._singleTransform) {
        return this._transformPayload(assignWeak(payload, inheritPayload));
      } else {
        //noinspection JSCheckFunctionSignatures
        return assignWeak(this._transformPayload(payload), inheritPayload);
      }
    }
    return assignWeak(payload, inheritPayload);
  };

  /**
   * Runs available transformers on the given object.
   *
   * @param {Object} obj The object to apply transformers to.
   * @returns {Object} The given object.
   * @private
   */
  Logger.prototype._transformPayload = function (obj) {
    const transformer = this.transformer;
    for (let key in transformer) {
      //noinspection JSUnfilteredForInLoop
      if (obj.hasOwnProperty(key)) {
        //noinspection JSUnfilteredForInLoop
        obj[key] = transformer[key](obj[key]);
      }
    }
    return obj;
  };

}

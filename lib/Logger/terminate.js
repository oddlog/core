export default attach;

/*==================================================== Functions  ====================================================*/

function attach(Logger) {

  /**
   * Closes the logger and all its children. Calls the given callback when all remaining messages are flushed to their
   * destination.
   *
   * A closed logger will no longer pass messages to its target streams.
   *
   * @param {Boolean} [failSilent=true] Whether to silently ignore future incoming messages. If false, an Error will be
   *        thrown on further message method calls.
   * @param {Function} [cb] The callback.
   * @throws {Error} The logger has already been closed.
   * @throws {Error} The logger is not the root logger of the scope.
   * @returns {Logger} Identity.
   *
   * @see Logger#child
   */
  Logger.prototype.close = function (failSilent, cb) {
    const scope = this.scope;
    if (!this._ownsScope) {
      throw new Error(
          "The logger is a child without dedicated scope. It cannot be closed, use Logger#close on the root logger " +
          "instance instead."
      );
    }
    if (scope.closed) { throw new Error("Logger has already been closed."); }
    if (typeof failSilent === "function") {
      //noinspection JSValidateTypes
      cb = failSilent;
      failSilent = true;
    }
    failSilent = failSilent !== false;
    // detach all listeners
    for (let key in this.__processListeners) {
      if (this.__processListeners.hasOwnProperty(key)) {
        const arr = this.__processListeners[key];
        const _len = arr.length;
        for (let i = 0; i < _len; i++) { process.removeListener(key, arr[i]); }
      }
    }
    this.__processListeners = null;
    if (typeof cb === "function") { scope.once("end", cb); }
    scope._close(failSilent);
    // free up objects for garbage collection
    this.__lastLog = this.__mergedPayload = this._payload = null;
    this.transports = this.messageFormatter = this._parentPayload = null;
    return this;
  };

  /**
   * Calls process.exit after the logger has been closed.
   *
   * @param {Number|Boolean} [code=0] The exit code to use. If false, exit will be omitted. True results in exit code 1.
   * @returns {Logger} Identity.
   *
   * @see Logger#close
   */
  Logger.prototype.exit = function (code) {
    let done;
    if (code == null) {
      done = () => process.exit(0);
    } else if (code === true) {
      done = () => process.exit(1);
    } else if (typeof code === "number") {
      done = () => process.exit(code);
    } else {
      return this;
    }
    if (this.scope.closed) {
      if (this.scope.busyWrites) { this.scope.once("end", done); } else { done(); }
    } else {
      this.close(done);
    }
    return this;
  };

}

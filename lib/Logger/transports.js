import {createTransport} from "../services/transports";

/*===================================================== Exports  =====================================================*/

export default attach;

/*==================================================== Functions  ====================================================*/

function attach(Logger) {

  /**
   * Creates a new Transport from the given options (utilizing {@link createTransport}) and adds it to active transports
   * of this logger.
   *
   * @param {Object} transportOptions The options to derive the Transport of.
   * @returns {Logger} Identity.
   */
  Logger.prototype.addTransport = function (transportOptions) {
    const transport = createTransport(transportOptions);
    if (transport == null) { return this; }
    if (typeof transport.attach === "function") { transport.attach(this); }
    this.transports.push(transport);
    this._updateMinTransportLevel(transport.minLevel());
    return this;
  };

  /**
   * Adds multiple transports as defined by the given options list.
   *
   * @param {(!Object)[]} transportOptionsList The list of options to derive each a Transport of.
   * @returns {Logger} Identity.
   * @see Logger#addTransport
   */
  Logger.prototype.addTransports = function (transportOptionsList) {
    let minLvl = this.__minTransportLevel;
    const transports = this.transports;
    const _len = transportOptionsList.length;
    for (let i = 0; i < _len; i++) {
      const transport = createTransport(transportOptionsList[i]);
      if (transport == null) { continue; }
      if (typeof transport.attach === "function") { transport.attach(this); }
      transports.push(transport);
      const tMinLevel = transport.minLevel();
      if (tMinLevel < minLvl) { minLvl = tMinLevel; }
    }
    this._updateMinTransportLevel(minLvl);
    return this;
  };

}

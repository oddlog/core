import Log from "@oddlog/record";
import {MAPPED as LEVELS, nameToValue} from "@oddlog/levels";
import {callerLocation, stubFalse, stubTrue} from "@oddlog/utils";

/*===================================================== Exports  =====================================================*/

export default attach;

/*==================================================== Functions  ====================================================*/

function attach(Logger) {

  /**
   * Registers an event handler to the `uncaughtException` event on the process.
   *
   * @param {String|Number} [level=FATAL] The level to use for the logs.
   * @param {Boolean|Number|Function} [exit=true] Whether to exit, the exit code or a function that returns either. If
   *        it's a function, it gets called with the error and might accept 2nd parameter callback function to operate
   *        async.
   * @returns {Logger} Identity.
   *
   * @see Logger#handleProcessEvents
   */
  Logger.prototype.handleUncaughtExceptions = function (level, exit) {
    if (typeof level !== "string" && typeof level !== "number") {
      if (level != null) { exit = level; }
      level = LEVELS.fatal;
    }
    if (exit == null) { exit = stubTrue; }
    this.handleProcessEvents("uncaughtException", "Uncaught exception", level, exit, "err");
    return this;
  };

  /**
   * Registers an event handler to the `unhandledRejection` event on the process.
   *
   * @param {String|Number} [level=ERROR] The level to use for the logs.
   * @param {Boolean|Number|Function} [exit=false] Whether to exit, the exit code or a function that returns either. If
   *        it's a function, it gets called with the error and might accept 2nd parameter callback function to operate
   *        async.
   * @returns {Logger} Identity.
   *
   * @see Logger#handleProcessEvents
   */
  Logger.prototype.handleUnhandledRejections = function (level, exit) {
    if (typeof level !== "string" && typeof level !== "number") {
      if (level != null) { exit = level; }
      level = LEVELS.error;
    }
    this.handleProcessEvents("unhandledRejection", "Unhandled rejection.", level, exit, "err");
    return this;
  };

  /**
   * Registers an event handler to the `warning` event on the process.
   *
   * @param {String|Number} [level=WARN] The level to use for the logs.
   * @param {Boolean|Number|Function} [exit=false] Whether to exit, the exit code or a function that returns either. If
   *        it's a function, it gets called with the error and might accept 2nd parameter callback function to operate
   *        async.
   * @returns {Logger} Identity.
   *
   * @see Logger#handleProcessEvents
   */
  Logger.prototype.handleWarnings = function (level, exit) {
    if (typeof level !== "string" && typeof level !== "number") {
      if (level != null) { exit = level; }
      level = LEVELS.warn;
    }
    this.handleProcessEvents("warning", "Process warning.", level, exit);
    return this;
  };

  /**
   * Registers an event handler to the given event on `process` that logs logs the event data.
   *
   * @param {String|String[]} eventName The name of the event to watch for on the process.
   * @param {String|Function} message The message to log. If a function is provided it gets called with the event data
   *        (1st parameter) and the logger instance (2nd parameter); It is expected to return a String.
   * @param {String|Number} level The logging level.
   * @param {Boolean|Number|Function} [exit=false] Whether to exit the process after event occurred, the exit code or a
   *        function that returns either. If it's a function, it gets called with the error and might accept 2nd
   *        parameter callback function to operate async.
   * @param {?String|Object} [key] The payload key to use for the event data if any. If an object is given, it will be
   *        used as payload. If not specified, the event data will be used as payload.
   * @returns {Logger} Identity.
   *
   * @see Logger#handleUncaughtExceptions
   * @see Logger#handleUnhandledRejections
   * @see Logger#handleWarnings
   */
  Logger.prototype.handleProcessEvents = function (eventName, message, level, exit, key) {
    const self = this;
    if (self.scope.closed) { throw new Error("Logger has already been closed."); }
    const source = self.scope.logSources ? callerLocation(3) : null;
    const listeners = this.__processListeners;
    if (typeof exit === "string" || exit === null && arguments.length < 5) {
      //noinspection JSValidateTypes
      key = exit;
      exit = stubFalse;
    } else if (typeof exit !== "function") {
      const value = exit;
      exit = () => value;
    }
    level = typeof level === "string" ? nameToValue(level) : level >= 0 ? level : -1;

    if (typeof eventName === "string") {
      add(eventName);
    } else {
      for (let i = 0; i < eventName.length; i++) { add(eventName[i]); }
    }

    return this;

    function add(name) {
      if (listeners.hasOwnProperty(name)) { listeners[name].push(handler); } else { listeners[name] = [handler]; }
      process.on(name, handler);
    }

    function handler(data) {
      if (!self.scope.closed) {
        // log generic message
        const text = typeof message === "function" ? message(data, self) : message;
        const payload = typeof key === "string" ? {[key]: data} : key === void 0 ? data : key;
        const args = payload === void 0 ? [true, text] : [true, payload, text];
        const msg = self.__lastLog = new Log(self, Date.now(), level, source, args, true);
        self.emit("message", msg);
        const transports = self.transports, _len = transports.length;
        for (let i = 0; i < _len; i++) { transports[i].write(msg); }
      }
      // handle exit procedure
      if (exit.length > 1) {
        exit(data, (value) => self.exit(typeof value === "number" ? value : !!value));
      } else {
        const value = exit(data);
        self.exit(typeof value === "number" ? value : !!value);
      }
    }
  };

}

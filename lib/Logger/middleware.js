export default attach;

/*==================================================== Functions  ====================================================*/

function attach(Logger) {

  /**
   * Creates a middleware function to be used by express.js compatible routers. The middleware attaches a child logger
   * to the request object (req.log).
   *
   * @param {Boolean} [ownPayload=this.ownChildPayloads] Whether the payload is allowed to be modified by the logger.
   * @param {Boolean} [ownChildPayloads=this.ownChildPayloads] Whether to keep the messages and children payload safe.
   *        If set to false, the payload of messages and children may get modified. This can be overwritten on child and
   *        log function calls.
   * @param {Boolean|String} [dedicatedScope=false] Whether to use a dedicated logger scope. Passed to
   *        {@link Logger#child}.
   * @param {?Object|Function<req,res>|Function<req,res,cb>} [payload={req,res}] Payload to add to the child. If a
   *        function is provided it is supposed to return the payload to use. It may operate async if it accepts the
   *        callback function (to be called with error and result parameter).
   * @returns {Function} The middleware for express.js compatible routers; Accepting request, response and callback.
   */
  Logger.prototype.mwExpress = function (ownPayload, ownChildPayloads, dedicatedScope, payload) {
    // todo refactor
    if (typeof ownPayload !== "boolean") {
      if (typeof ownPayload === "string") {
        dedicatedScope = ownPayload;
        payload = ownChildPayloads;
      } else {
        dedicatedScope = false;
        payload = ownPayload;
      }
      ownPayload = ownChildPayloads = this.ownChildPayloads;
    } else if (typeof ownChildPayloads !== "boolean") {
      if (typeof ownChildPayloads === "string") {
        payload = dedicatedScope;
        dedicatedScope = ownChildPayloads;
      } else {
        payload = ownChildPayloads;
        dedicatedScope = false;
      }
      ownChildPayloads = this.ownChildPayloads;
    } else if (typeof dedicatedScope !== "boolean" && typeof dedicatedScope !== "string") {
      payload = dedicatedScope;
      dedicatedScope = false;
    }
    if (payload === void 0) {
      return (req, res, next) => {
        req.log = this.child(ownPayload, ownChildPayloads, {req, res}, dedicatedScope);
        next();
      };
    } else if (typeof payload === "function") {
      if (payload.length > 2) {
        return (req, res, next) => {
          payload(req, res, (err, payload) => {
            req.log = this.child(ownPayload, ownChildPayloads, payload, dedicatedScope);
            next(err);
          });
        };
      } else {
        return (req, res, next) => {
          req.log = this.child(ownPayload, ownChildPayloads, payload(req, res), dedicatedScope);
          next();
        };
      }
    } else {
      return (req, res, next) => {
        req.log = this.child(ownPayload, ownChildPayloads, payload, dedicatedScope);
        next();
      };
    }
  };

}

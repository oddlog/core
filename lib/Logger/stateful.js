import {MAPPED as LEVELS, nameToValue} from "@oddlog/levels";
import {self as selfFn} from "@oddlog/utils";

import {log} from "./log";

/*===================================================== Exports  =====================================================*/

export default attach;

/*==================================================== Functions  ====================================================*/

function attach(Logger) {

  Logger.prototype.log = selfFn; // overwrite as min transport level is lowered and level is modified

  /**
   * Sets the current active level to the given value.
   * This is useful in conjunction with {@link Logger#log}.
   *
   * @param {Number|String} level The level. It should match a valid {@link LEVELS} key if a String is provided.
   * @returns {Logger} Identity.
   * @see Logger#done
   * @see Logger#log
   * @see Logger#getLevel
   */
  Logger.prototype.setLevel = function (level) {
    this._level = typeof level === "string" ? nameToValue(level) : level;
    this.log = this._level >= this.__minTransportLevel ? log : selfFn;
    return this;
  };

  /**
   * Returns the current active level.
   *
   * @returns {Number} The active level.
   * @see Logger#setLevel
   */
  Logger.prototype.getLevel = function () { return this._level; };

  /**
   * Resets the active level to default value or option used for constructor.
   * This is supposed to be called after {@link Logger#log} calls.
   *
   * @returns {Logger} Identity.
   * @see Logger#setLevel
   * @see Logger#log
   */
  Logger.prototype.done = function () {
    this._level = this._defaultLevel;
    this.log = this._level >= this.__minTransportLevel ? log : selfFn;
    return this;
  };


  for (let name in LEVELS) {
    const nameCapital = name[0].toUpperCase() + name.substring(1);
    const value = LEVELS[name];

    /**
     * Sets the current active level according to function name. This is useful in conjunction with {@link Logger#log}.
     *
     * @returns {Logger} Identity.
     */
    Logger.prototype["set" + nameCapital] = function () {
      this._level = value;
      this.log = value >= this.__minTransportLevel ? log : selfFn;
      return this;
    };
  }

}

import EventEmitter from "events";
import {hostname, release} from "os";
import {format as utilFormat} from "util";
import {SCHEMA_VERSION} from "@oddlog/record";
import {DEFAULT_VALUE as DEFAULT_LEVEL, getLevelValue, LIST as LEVEL_LIST} from "@oddlog/levels";
import {clone, escape, identity} from "@oddlog/utils";

import * as STANDARD_TRANSFORMER from "../services/transformer";
import LoggerScope from "../LoggerScope";

import protoChild from "./child";
import protoEventHandler from "./event-handler";
import protoPayload from "./payload";
import protoStateful from "./stateful";
import protoStateless from "./stateless";
import protoTerminate from "./terminate";
import protoTransports from "./transports";

let defaultMetaString = null; // lazy singleton

/*===================================================== Exports  =====================================================*/

export default Logger;

export {createLogger};

/*===================================================== Classes  =====================================================*/

/**
 * Creates a new Logger instance. The Logger manages transports and provides logging methods.
 *
 * This function is verbose in arguments in order to avoid computation within the constructor as different computations
 * are necessary for child and root creation. Use {@link createLogger} for instantiation.
 *
 * The following Events are emitted:
 *
 *  * "log" {Log} log - Whenever logging methods are used.
 *  * "child" {Logger} child - Whenever a child logger got created.
 *  * "error" {Error} error - Whenever an error occurs.
 *
 * Additional events that regard children as well are emitted on {@link Logger#scope}.
 *
 * For transport events, take a look at the docs.
 *
 * @param {String} prefix The prefix of log record strings.
 * @param {?Object} [payload] The payload to inherit to all children and messages.
 * @param {?Object} [parentPayload] The already transformed payload of the parent.
 * @param {?Object} [transformer] A key/function mapping of transformer functions to call with payload values.
 * @param {Number} level The default level to fallback to for the {@link Logger#log} function.
 * @param {Boolean} shy Whether to enable some library-sane default options for transports.
 * @param {Boolean} ownChildPayloads Whether the payload passed to child and log functions is allowed to be modified
 *        by the logger. This can be overwritten on a per-child/-message basis.
 * @param {Boolean} singleTransform Whether transformations shall only be executed at message level. If set to
 *        false, logger level payload transformations will be cached.
 * @param {?Boolean} logSources Whether to add the source-code location of the calling function to log messages of
 *        child logger scopes.
 * @param {Function} messageFormatter The formatter function to use for primary message texts.
 * @param {LoggerScope} scope The scope to use (shared between the logger and its children).
 * @param {Boolean} ownsScope Whether the instance is the root logger of the given LoggerScope.
 * @constructor
 */
function Logger(
    prefix, payload, parentPayload, transformer, level, shy, ownChildPayloads, singleTransform, logSources,
    messageFormatter, scope, ownsScope
) {
  EventEmitter.call(this);

  this.scope = scope;
  this.transformer = transformer;
  this.transports = [];
  this.ownChildPayloads = ownChildPayloads;
  this.messageFormatter = messageFormatter;

  this._logSources = logSources;
  this._defaultLevel = level;
  this._level = level;
  this._prefix = prefix;
  this._payload = payload;
  this._shy = shy;
  this._singleTransform = singleTransform;
  this._parentPayload = parentPayload;
  this._ownsScope = ownsScope;

  this.__minTransportLevel = LEVEL_LIST.length;
  this.__lastLog = null;
  this.__mergedPayload = null;
  this.__hasMergedPayload = false;
  this.__processListeners = {};
}

// extend EventEmitter
for (let key in EventEmitter.prototype) { // noinspection JSUnfilteredForInLoop
  Logger.prototype[key] = EventEmitter.prototype[key];
}

// attach prototype functions from within other files
protoChild(Logger);
protoEventHandler(Logger);
protoPayload(Logger);
protoStateful(Logger);
protoStateless(Logger);
protoTerminate(Logger);
protoTransports(Logger);

/*==================================================== Functions  ====================================================*/

/**
 * This function provides an interface to easily create Logger instances without the need to specify every option and
 * handle Transport creation.
 *
 * @param {String} name The name for the new logger.
 * @param {?Object} [options] The options to define the logger:
 * <ul>
 *   <li>`{(?Object)[]} [transports=[{type:"stream",level:TRACE}]]` A list of options to derive transports of.</li>
 *   <li>`{Object} [transformer={fn,err,req,res}]` Transformer mapping to apply to message payload.</li>
 *   <li>`{Boolean} [ownPayload=false]` Whether the `payload` may be modified by the Logger.</li>
 *   <li>`{Boolean} [ownChildPayloads=false]` Whether payloads of children and messages may be modified by the
 *       Logger. This can be overwritten respectively within child and logging methods.</li>
 *   <li>`{Boolean} [singleTransform=true]` Whether transformations shall only be executed at message level. If set to
 *       false, logger level payload transformations will be cached. Caching can speed up the logging (especially for
 *       computational complex transformers), but will freeze the value of logger payload for all its messages.</li>
 *   <li>`{?Array|Object} [meta=[platform,host,pid]]` Meta information to add to messages. The default values are
 *       os.release(), os.hostname() and process.pid. Set to `null` to disable meta information.</li>
 *   <li>`{?String} [typeKey="_type"]` The key to use for a payloads (nested) type identifications. Supported types by
 *       oddlog can be specified within any payload data at this key. Such types are "error", "value" and "plain". If
 *       set to null, only some keys (such as "err") will identify a default type; Everything else is handled like the
 *       type "plain".</li>
 *   <li>`{?Boolean} [logSources]` Whether to add the source code location of the caller to messages. This might slow
 *       down your application and is not recommended for production. If not set, it defaults to `false` iff no
 *       environment variable (DEBUG/TRACE) is matching.</li>
 *   <li>`{?Function} [messageFormatter=util.format]` The formatting function to use for primary message texts.</li>
 *   <li>`{Number|String} [level=INFO]` The level to log with using the {@link Logger#log} method.</li>
 *   <li>`{Boolean} [shy=false]` Whether to set some sane library default options for transports (fallback-level: WARN
 *       and format: simple). Usage of DEBUG/TRACE environment variables that match this logger nullifies the effect.
 *       This option is intended to be used by libraries (at least in production).</li>
 * </ul>
 * @param {?Object} [payload] Payload to add to messages of the logger and its children.
 * @returns {Logger} The new Logger instance.
 * @see createTransport
 */
function createLogger(name, options, payload) {
  if (options == null) { options = {}; }

  const prefix = getRootLoggerPrefix(options);
  const ownedPayload = getPayload(options, payload);
  const transformer = options.hasOwnProperty("transformer") ? options.transformer : STANDARD_TRANSFORMER;
  const level = getLevelValue(options.level, DEFAULT_LEVEL);
  const shy = !!options.shy;
  const ownChildPayloads = !!options.ownChildPayloads;
  const singleTransform = options.hasOwnProperty("singleTransform") ? !!options._singleTransform : true;
  const logSources = options.logSources;
  const messageFormatter = getMessageFormatter(options);
  const scope = new LoggerScope(name, logSources);

  const logger = new Logger(
      prefix, ownedPayload, void 0, transformer, level, shy, ownChildPayloads, singleTransform, logSources,
      messageFormatter, scope, true
  );

  initTransports(logger, options);

  return logger;
}

function initTransports(logger, options) {
  if (options.hasOwnProperty("transports")) {
    if (options.transports != null) { logger.addTransports(options.transports); }
  } else {
    logger.addTransport({type: "stream", level: exports.TRACE});
  }
}

function getPayload(options, payload) { return options.ownPayload || payload == null ? payload : clone(payload); }

function getMessageFormatter(options) {
  if (options.hasOwnProperty("messageFormatter")) {
    return typeof options.messageFormatter === "function" ? options.messageFormatter : identity;
  } else {
    return utilFormat;
  }
}

function getRootLoggerPrefix(options) {
  const typeKeyString = options.hasOwnProperty("typeKey") ? "\"" + escape(options.typeKey) + "\"" : "\"_type\"";
  const metaString = options.hasOwnProperty("meta") ? JSON.stringify(options.meta) : getDefaultMetaString();
  return "[" +
      SCHEMA_VERSION + "," +
      typeKeyString + "," +
      metaString + "," +
      "\"";
}

function getDefaultMetaString() {
  if (defaultMetaString != null) { return defaultMetaString; }
  return defaultMetaString = "[\"" + escape(release()) + "\",\"" + escape(hostname()) + "\"," + process.pid + "]";
}

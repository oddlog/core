import {clone} from "@oddlog/utils";

import LoggerScope, {SCOPE_NAME_SEPARATOR} from "../LoggerScope";

/*===================================================== Exports  =====================================================*/

export default attach;

/*==================================================== Functions  ====================================================*/

function attach(Logger) {

  /**
   * Creates a new child logger, holding the additional payload.
   *
   * @param {Boolean} [ownPayload=this.ownChildPayloads] Whether the payload is allowed to be modified by the logger.
   * @param {Boolean} [ownChildPayloads=this.ownChildPayloads] Whether to keep the messages and children payload safe.
   *        If set to false, the payload of messages and children may get modified. This can be overwritten on child and
   *        log function calls.
   * @param {String|Boolean} [dedicatedChildScope=false] If set, use a dedicated scope (with the string value as
   *        name-suffix, if any).
   * @param {?Object} [payload] Payload to add to the child.
   * @returns {Logger} The child.
   */
  Logger.prototype.child = function (ownPayload, ownChildPayloads, dedicatedChildScope, payload) {
    // sanitize arguments
    if (typeof ownPayload === "object") {
      dedicatedChildScope = false;
      payload = ownPayload;
      ownPayload = ownChildPayloads = this.ownChildPayloads;
    } else if (typeof ownPayload === "string") {
      dedicatedChildScope = ownPayload;
      payload = ownChildPayloads;
      ownPayload = ownChildPayloads = this.ownChildPayloads;
    } else if (typeof ownChildPayloads === "object") {
      dedicatedChildScope = false;
      payload = ownChildPayloads;
      ownChildPayloads = this.ownChildPayloads;
    } else if (typeof ownChildPayloads === "string") {
      payload = dedicatedChildScope;
      dedicatedChildScope = ownChildPayloads;
      ownChildPayloads = this.ownChildPayloads;
    } else if (typeof payload === "boolean" || typeof payload === "string") {
      dedicatedChildScope = payload;
      payload = void 0;
    }
    // gain payload ownership
    if (!ownPayload && payload != null) { payload = clone(payload); }
    // figure out child scope
    const hasDedicatedScope = dedicatedChildScope === true || typeof dedicatedChildScope === "string";
    const childScope = getChildScope(this.scope, hasDedicatedScope, dedicatedChildScope, this._logSources);
    // create child logger
    const child = new Logger(
        this._prefix, payload, this.getPreparedPayload(), this.transformer, this._defaultLevel, this._shy,
        ownChildPayloads, this._singleTransform, this._logSources, this.messageFormatter, childScope, hasDedicatedScope
    );
    // create and attach child transports
    attachChildTransports(this.transports, child, hasDedicatedScope);
    // emit successful child creation
    this.emit("child", child);
    return child;
  };

}

function getChildScope(scope, hasChildScope, nameSuffix, logSources) {
  if (hasChildScope) {
    if (typeof nameSuffix !== "string") { return new LoggerScope(scope.name, logSources, scope); }
    return new LoggerScope(scope.name + SCOPE_NAME_SEPARATOR + nameSuffix, logSources, scope);
  } else {
    return scope;
  }
}

function attachChildTransports(transports, child, hasDedicatedScope) {
  let minLvl = child.__minTransportLevel;
  const _len = transports.length;
  for (let i = 0; i < _len; i++) {
    const tMinLevel = addChildTransport(transports[i], child, hasDedicatedScope).minLevel();
    if (tMinLevel < minLvl) { minLvl = tMinLevel; }
  }
  child._updateMinTransportLevel(minLvl);
}

function addChildTransport(transport, child, hasDedicatedScope) {
  if (
      hasDedicatedScope ||
      (typeof transport.forceChild === "function" ? transport.forceChild() : transport.forceChild)
  ) {
    transport = transport.child(hasDedicatedScope);
    if (typeof transport.attach === "function") { transport.attach(child); }
  }
  child.transports.push(transport);
  return transport;
}

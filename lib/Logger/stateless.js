import Log from "@oddlog/record";
import {LIST as LEVEL_LIST, MAPPED as LEVELS} from "@oddlog/levels";
import {callerLocation, self as selfFn} from "@oddlog/utils";

import {log} from "./log";

const LOGGING_METHODS = [];

/*===================================================== Exports  =====================================================*/

export default attach;

/*==================================================== Functions  ====================================================*/

function attach(Logger) {

  for (let name in LEVELS) {
    const nameCapital = name[0].toUpperCase() + name.substring(1);
    const value = LEVELS[name];

    Logger.prototype[name] = selfFn; // overwrite as min transport level is lowered

    /**
     * Returns whether the minimum level to process logs is at or below the level within the method name.
     *
     * @returns {Boolean} Whether a message of this level would get processed.
     */
    Logger.prototype["is" + nameCapital] = function () { return this.__minTransportLevel <= value; };
  }

  /**
   * To be called after transports have been added.
   * Updates the logger instance to provide all necessary logging functions instead of pre-existing noop references.
   *
   * @param {Number} level The new minimum level to require actual logging functions.
   * @private
   */
  Logger.prototype._updateMinTransportLevel = function (level) {
    if (this.__minTransportLevel <= level) { return; }
    let lvl, minLvl;
    // lvl = min(0, ceil(transport.minLevel()))
    if (level >= 0) {
      // fast bitwise ceil implementation
      minLvl = level;
      lvl = level << 0;
      lvl = lvl === level ? lvl : lvl + 1;
    } else {
      lvl = minLvl = 0;
    }
    let value = this.__minTransportLevel;
    while (value > lvl) {
      const name = LEVEL_LIST[--value];
      this[name] = LOGGING_METHODS[value];
    }
    this.log = this._level >= minLvl ? log : selfFn;
    this.__minTransportLevel = minLvl;
  };

  /**
   * Alias for `error({err})`.
   *
   * @param {Error} err The error.
   * @param {?String} [message] The log message.
   * @returns {Logger} Identity.
   */
  Logger.prototype.err = function (err, message) {
    if (this.__minTransportLevel <= LEVELS.error) { return this.error(true, {err}, message); }
    return this;
  };

}

for (let name in LEVELS) {
  const value = LEVELS[name];

  /**
   * Logs a message with the level according to function name.
   *
   * @param {*} args The arguments (in this order):
   * <ul>
   *   <li>`{Boolean} [ownPayload]` Set to overwrite ownChildPayloads option of the logger.</li>
   *   <li>`{?Object} [payload]` Payload data to add to the message.</li>
   *   <li>`{String} [message]` String to use as primary message.</li>
   *   <li>`{String...|Object} [formatValues]` Additional arguments to format the primary message.</li>
   * </ul>
   * @returns {Logger} Identity.
   * @throws {Error} The logger got closed already and silent has been set to false ({@link Logger#close}).
   */
  LOGGING_METHODS[value] = function (...args) {
    const scope = this.scope;
    if (scope.closed) {
      const err = new Error("Received log record on closed Logger.");
      this.emit("log-error", {err, args});
      if (scope._throw) { throw err; }
      return this;
    }
    const source = scope.logSources ? callerLocation(3) : null;
    const log = this.__lastLog = new Log(this, Date.now(), value, source, args, true);
    this.emit("log", log);
    const transports = this.transports, _len = transports.length;
    for (let i = 0; i < _len; i++) { transports[i].write(log); }
    return this;
  };
}

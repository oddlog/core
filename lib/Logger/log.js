import Log from "@oddlog/record";
import {callerLocation} from "@oddlog/utils";

/*===================================================== Exports  =====================================================*/

export {log};

/*==================================================== Functions  ====================================================*/

/**
 * Logs a message with the current active level.
 *
 * @param {*} args The arguments (in this order):
 * <ul>
 *   <li>`{Boolean} [ownPayload]` Set to overwrite ownChildPayloads option of the logger.</li>
 *   <li>`{?Object} [payload]` Payload data to add to the message.</li>
 *   <li>`{String} [message]` String to use as primary message.</li>
 *   <li>`{String...|Object} [formatValues]` Additional arguments to format the primary message.</li>
 * </ul>
 * @returns {Logger} Identity.
 * @throws {Error} The logger got closed already and silent has been set to false ({@link Logger#close}).
 * @see Logger#setLevel
 * @see Logger#done
 */
function log(...args) { /* eslint no-invalid-this:"off" */
  const scope = this.scope;
  if (scope.closed) {
    const err = new Error("Received log record on closed Logger.");
    this.emit("log-error", {err, args});
    if (scope._throw) { throw err; }
    return this;
  }
  const source = scope.logSources ? callerLocation(3) : null;
  const log = new Log(this, Date.now(), this._level, source, args, true);
  this.__lastLog = log;
  this.emit("log", log);
  const transports = this.transports, _len = transports.length;
  for (let i = 0; i < _len; i++) { transports[i].write(log); }
  return this;
}

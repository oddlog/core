import {check as isStreamOptions, create as createStreamTransport} from "@oddlog/transport-stream";

const MAPPED = {};
const CHECK = [];

defineType("instance", createInstanceTransport, isInstanceOptions);
defineType("stream", createStreamTransport, isStreamOptions);

/*===================================================== Exports  =====================================================*/

export {MAPPED, createTransport, defineType, getFallbackType};

/*==================================================== Functions  ====================================================*/

function createInstanceTransport(options) { return options.instance; }

function isInstanceOptions(options) { return options.hasOwnProperty("instance"); }

/**
 * Define a new type of Transport.
 *
 * @param {String} key The identifier of the type.
 * @param {Function} createFn The function that creates instances of the type.
 * @param {Function<Boolean|Number>} [checkFn] The function to check whether to use the type derived from options. Does
 *        not need to check for `type` attribute. The checkFn may return a number for priority of usage, or a boolean
 *        (`true := 0`).
 * @param {Boolean} [overwrite=false] Whether to overwrite, if a type with the same key already exists.
 */
function defineType(key, createFn, checkFn, overwrite) {
  if (typeof checkFn === "boolean") {
    overwrite = checkFn;
  } else if (typeof checkFn === "function") {
    CHECK.push({type: key, test: checkFn});
  }
  if (MAPPED.hasOwnProperty(key) && !overwrite) { throw new Error("Type '" + key + "' already registered."); }
  MAPPED[key] = createFn;
}

/**
 * Derives the transport type from transport definition options.
 *
 * @param {Object} options The options to derive the transport type of.
 * @returns {String} The transport type to use for the passed options.
 * @private
 */
function getFallbackType(options) {
  let maxPriority = Number.NEGATIVE_INFINITY;
  let type = null;
  for (let i = 0; i < CHECK.length; i++) {
    const priority = CHECK[i].test(options);
    if (priority === false) { continue; }
    const p = typeof priority === "number" ? priority : 0;
    if (maxPriority < p) {
      maxPriority = p;
      type = CHECK[i].type;
    }
  }
  if (type != null) { return type; }
  throw new Error("Transport type could not be determined.");
}

/**
 * Creates a new Transport instance according to given options.
 * If the options contain a identifying key (e.g. `stream`, `path`, `trigger`), the type field can be omitted.
 * Otherwise the type key must be an identifier of a Transport class.
 * The options are passed to the constructor of the Transport class for type-specific impacts.
 *
 * @param {Object} options The options to derive the Transport of.
 * @returns {Object} The Transport instance.
 * @see defineType
 */
function createTransport(options) {
  if (options == null) { return null; }
  const type = options.type || getFallbackType(options);
  const transport = MAPPED[type];
  if (typeof transport !== "function") { throw new TypeError("Unknown transport type: " + type); }
  return transport(options, createTransport);
}

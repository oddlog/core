import util from "util";
import EventEmitter from "events";
import {isAnySetAndTest as isEnvMatching} from "@oddlog/environment";
import {escape} from "@oddlog/utils";

/*===================================================== Exports  =====================================================*/

export default LoggerScope;

export const SCOPE_NAME_SEPARATOR = ":";

/*===================================================== Classes  =====================================================*/

/**
 * Creates a new logger scope that is to be shared between Logger instances and their children except a child demands to
 * use a dedicated scope. The scope requires transports to notify about the status of asynchronous write operation. This
 * allows to listen for related events.
 *
 * The following Events are emitted:
 *
 *  * "drain" - Whenever all asynchronous messages have been flushed.
 *  * "close" - When the scope is getting closed.
 *  * "end" - When all messages have been flushed after the scope has been closed.
 *
 * @param {String} name The name of the logger scope.
 * @param {?Boolean} [logSources] Whether to use source location logging for loggers of this scope. If not set, it
 *        defaults to `false` iff no environment variable is matching.
 * @param {LoggerScope} [parent] The parent scope.
 * @constructor
 * @private
 */
function LoggerScope(name, logSources, parent) {
  EventEmitter.call(this);
  this.name = name;
  this.logSources = logSources == null ? isEnvMatching(name) : logSources;
  this.closed = false;
  this.busyWrites = 0;
  this._prefix = escape(name) + "\",";
  this._parent = parent;
  this._throw = false;
  if (parent != null) {
    this._parentOnClose = () => this._close(!parent._throw);
    parent.on("close", this._parentOnClose); // prevents garbage collection of this until close() is called
  }
}

// extend EventEmitter
util.inherits(LoggerScope, EventEmitter);


/**
 * To be called when a Transport has started an asynchronous message delivery.
 */
LoggerScope.prototype.down = function () {
  this.busyWrites++;
  if (this._parent != null) { this._parent.down(); }
};

/**
 * To be called when a Transport has finished an asynchronous message delivery. It is also supposed to be called
 * during a process-task that is not triggered by the user application (e.g. process.nextTick).
 */
LoggerScope.prototype.up = function () {
  if (!--this.busyWrites) { this.emit("drain"); }
  if (this._parent != null) { this._parent.up(); }
};

/**
 * Alias for {@link LoggerScope#up} for multiple message deliveries.
 *
 * @param {Number} num The amount of message deliveries that have been finished.
 */
LoggerScope.prototype.upTimes = function (num) {
  if (!(this.busyWrites -= num)) { this.emit("drain"); }
  if (this._parent != null) { this._parent.upTimes(num); }
};

/**
 * Closes this logger scope and all children.
 *
 * @param {Boolean} failSilent Whether to silently ignore future incoming logs.
 * @private
 */
LoggerScope.prototype._close = function (failSilent) {
  this.closed = true;
  this._throw = !failSilent;
  if (this._parent != null) {
    this._parent.removeListener("close", this._parentOnClose); // release this from parent for garbage collection
  }
  this.emit("close");
  const asyncCb = () => this.emit("end");
  if (this.busyWrites) { this.once("drain", () => process.nextTick(asyncCb)); } else { process.nextTick(asyncCb); }
};
